import org.junit.Assert;
import org.junit.Test;
import sort.Sort;
import sort.SortImpl;

import java.util.Arrays;

public class SortTest {

    private Sort sort = new SortImpl();

    @Test
    public void testSort_InputInReversOrder_OutputIsSorted() {
        // Arrange
        int[] inp =  new int[] {1, 0, -1};

        // Act
        int[] actualResult = sort.sort(inp);

        // Assert
        Assert.assertArrayEquals(Arrays.stream(inp).sorted().toArray(), actualResult);
    }

    @Test
    public void testSort_InputIsNull_ThrowsNPE() {
        // Arrange
        int[] inp =  null;

        // Act
        int[] actualResult = sort.sort(inp);

        // Assert
        Assert.assertNull(actualResult);
    }

}
