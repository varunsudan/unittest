package sort;

@FunctionalInterface
public interface Sort {

    int[] sort(int[] input);
}
