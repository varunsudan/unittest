package sort;

import java.util.Arrays;

public class SortImpl implements Sort {

    @Override
    public int[] sort(int[] input) {
        if(input != null) {
            return Arrays.stream(input).sorted().toArray();
        }
        return input;
    }
}
