# UnitTest
Help understand key principles behind good unit test cases.

## Key principles of writing good unit test
- Structure your tests to have following sections : Arrange, Act, Assert (i.e. set up for testing code , invoke  method under test, check the expected result)
- Be as  descriptive with your method name i.e. test{methodName}{Delimiter}{ConditionBeingTested}{Delimiter}{ExpectedResult} -> testSort_WhenInputIsNull_ThrowsException
- Try to limit one assert per test method. Multiple asserts is all right as long as they are testing one idea/concept.
- No Interdependence between test cases. Unit test cases should be able to run in isolation in any order.
- Complex setup in @Before block or  within "Arrange" section of unit test is a code smell.
- Test Interfaces not implementation. 

## Run

## Interesting reads



